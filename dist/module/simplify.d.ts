export default function simplify(points: [number, number][], tolerance: number, highestQuality: boolean): [number, number][];
